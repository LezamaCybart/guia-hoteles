$(function () {
 $("[data-toggle='tooltip']").tooltip();
 $('.carousel').carousel({
    interval: 5000
 });
 $('#contacto').on('show.bs.modal', function (e) {
    console.log('el modal contacto se está mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', true);

 });
 $('#contacto').on('shown.bs.modal', function (e) {
    console.log('el modal contacto se mostró');
 });
 $('#contacto').on('hide.bs.modal', function (e) {
    console.log('el modal contacto se esconde');
 });
 $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('el modal contacto se escondió');
    $('#contactoBtn').prop('disabled', false);
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
 });
});
